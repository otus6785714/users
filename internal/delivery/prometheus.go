package delivery

import (
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var requestTotal = promauto.NewCounterVec(
	prometheus.CounterOpts{
		Name: "http_requests_total",
	},
	[]string{"status", "path", "method"},
)

var responseStatus = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Name: "response_status",
		Help: "Status of HTTP response",
	},
	[]string{"status", "path", "method"},
)

var httpDuration = promauto.NewHistogramVec(prometheus.HistogramOpts{
	Name: "http_response_time_seconds",
	Help: "Duration of HTTP requests.",
}, []string{"path", "method"})

func initPrometheus() {
	prometheus.Register(responseStatus)
	prometheus.Register(httpDuration)
	prometheus.Register(requestTotal)
}

func prometheusMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {

		path := c.FullPath()

		timer := prometheus.NewTimer(httpDuration.WithLabelValues(path, c.Request.Method))
		c.Next()

		statusCode := c.Writer.Status()

		requestTotal.WithLabelValues(strconv.Itoa(statusCode), path, c.Request.Method).Inc()

		responseStatus.WithLabelValues(strconv.Itoa(statusCode), path, c.Request.Method).Inc()

		timer.ObserveDuration()
	}
}
